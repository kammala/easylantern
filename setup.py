from setuptools import setup, find_packages

setup(
    name='easylantern',
    version='0.1d2',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=['tornado>=4'],
    extras_require={'Qt5': ['PyQt5']}
)
