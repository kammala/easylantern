__all__ = []

import re
import struct

from . import SUPPORTED_TYPES, HEADER_SIZE


class RegisterCommandMeta(type):
    """ This metaclass register all classes created with it in module __all__(to import them all with *)
        and in SUPPORTED_TYPES from proto module. """
    def __init__(cls, name, bases, attrs):
        if 'code' not in attrs or not isinstance(attrs['code'], int):
            raise TypeError('Cannot create {} command without type code'.format(name))
        SUPPORTED_TYPES[attrs['code']] = cls
        __all__.append(name)
        # ??? how to add default class params ???
        super(RegisterCommandMeta, cls).__init__(name, bases, attrs)


class Command(metaclass=RegisterCommandMeta):
    """ Base Command class with default parameters """
    code = 0x00
    body_fmt = ''
    body = []

    def __repr__(self):
        return '{}'.format(self.__class__.__name__)

    # this function is unnecessary but really cute
    def __len__(self):
        return self.length()

    @classmethod
    def length(cls):
        return HEADER_SIZE + struct.calcsize(cls.body_fmt or '')


class On(Command):
    """ Singleton class for protocol command ON. """
    __instance = None

    code = 0x12

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = super(On, cls).__new__(cls, *args, **kwargs)
        return cls.__instance


class Off(Command):
    """ Singleton class for protocol command OFF. """
    __instance = None

    code = 0x13

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = super(Off, cls).__new__(cls, *args, **kwargs)
        return cls.__instance


class Color(Command):
    code = 0x20
    body_fmt = '>BBB'

    _color_format = r'\s*(#)?(?P<r>[\da-fA-F]{2})(?P<g>[\da-fA-F]{2})(?P<b>[\da-fA-F]{2})\s*'
    _color_pat = re.compile(_color_format)

    def __init__(self, *args):
        style = type(args[0])
        assert all(isinstance(arg, style) for arg in args), 'All argument should be same type'
        if style == int:
            assert len(args) == 3, 'Exactly 3 arguments required'
            assert all(0 <= arg < 256 for arg in args), 'Arguments should be in [0..255] range'
            self.r, self.g, self.b = args
        elif style == float:
            assert len(args) == 3, 'Exactly 3 arguments required'
            assert all(0.0 <= arg <= 1.0 for arg in args), 'Arguments should be in [0.0..1.0] range'
            self.r, self.g, self.b = [int(256 * arg) for arg in args]
        elif style == str:
            assert len(args) == 1, 'Exactly 1 arguments required'
            assert self._color_pat.match(args[0]), 'Incorrect color format'
            mo = self._color_pat.match(args[0])
            for name, val in mo.groupdict().items():
                setattr(self, name, int(val, 16))
        else:
            print(style)
            raise TypeError('Incorrect arguments')

    @property
    def body(self):
        return self.r, self.g, self.b

    def __repr__(self):
        return '{} #{:02x}{:02x}{:02x}'.format(self.__class__.__name__, self.r, self.g, self.b)