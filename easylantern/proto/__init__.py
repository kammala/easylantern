"""
Lantern Control Protocol serialization module.
Contains serialization methods, protocol specific constants and submodule `commands` with supported commands.
"""

__all__ = ['save', 'load', 'LCPError', 'ParseError', 'UnknownCommand']

import struct


class LCPError(Exception): pass

class ParseError(LCPError): pass


class UnknownCommand(LCPError):
    def __init__(self, msg, *, bodylen):
        super(UnknownCommand, self).__init__(msg)
        self.body_length = bodylen

HEADER = '>BH'
HEADER_SIZE = struct.calcsize(HEADER)
SUPPORTED_TYPES = dict()


def save(command):
    buffer = bytearray(len(command))
    struct.pack_into(HEADER, buffer, 0, command.code, len(command) - HEADER_SIZE)
    struct.pack_into(command.body_fmt, buffer, HEADER_SIZE, *command.body)
    return bytes(buffer)


# todo: to generator
def load(data):
    if not isinstance(data, (bytes, bytearray)):
        raise TypeError('Data should be bytes or bytearray')
    type_code, body_length = struct.unpack_from(HEADER, data)
    if type_code in SUPPORTED_TYPES:
        type_cls = SUPPORTED_TYPES[type_code]
        if body_length + HEADER_SIZE != type_cls.length():
            raise ParseError('Bad length: got {}, expected {}'.format(body_length, type_cls.length() - HEADER_SIZE))
        if body_length:
            if len(data) >= type_cls.length():
                values = struct.unpack_from(type_cls.body_fmt, data, HEADER_SIZE)
                command = type_cls(*values)
            else:
                # not enough data
                command = None
        else:
            command = type_cls()
        return command, body_length
    else:
        raise UnknownCommand('Unknown command type code: {}'.format(type_code), bodylen=body_length)