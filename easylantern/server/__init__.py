import logging

from tornado import gen
from tornado.tcpserver import TCPServer

from easylantern import proto

logger = logging.getLogger(__name__)


class EasyLanternServer(TCPServer):
    def __init__(self, host, port):
        super(EasyLanternServer, self).__init__()
        self.client = None
        self.listen(port=port, address=host)

    @property
    def is_active(self):
        return self.client and not self.client.closed()

    def handle_stream(self, stream, address):
        logger.debug('New client tries to connect...')
        if self.is_active:
            logger.debug('Disconnect from old client')
            self.client.close()
        self.client = stream
        logger.info('New client connected')

    def stop(self):
        if self.is_active:
            self.client.close()
        super(EasyLanternServer, self).stop()

    @gen.coroutine
    def send(self, command):
        dump = proto.save(command)
        if self.is_active:
            yield self.client.write(dump)
        else:
            logger.debug('No active clients. Nothing will be sent.')