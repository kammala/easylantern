import logging
import sys

from easylantern.ui import cliparams
from easylantern.ui.server.cli.auto import AutoServerCLI
from easylantern.ui.server.cli.interactive import InteractiveServerCLI

logger = logging.getLogger('easylantern')

if __name__ == '__main__':
    parser = cliparams.LanternParamParser(module='server')
    parser.add_argument('--ui', default='interactive', choices=['interactive', 'auto'])
    parser.add_argument('-d', '--delay', default=3, type=int,
                        help='delay between auto generated commands. Use with `--ui auto`')
    params = parser.parse_args()
    cliparams.configLogger(logger, params.verbose, params.log)

    logger.info('Running server on {}:{}...'.format(params.host, params.port))

    if params.ui == 'interactive':
        ui = InteractiveServerCLI(prompt='[easylantern server cli]: ',
                                  host=params.host,
                                  port=params.port)
    elif params.ui == 'auto':
        ui = AutoServerCLI(host=params.host, port=params.port, sleep_interval=params.delay)
    else:
        sys.exit(1)
    ui()