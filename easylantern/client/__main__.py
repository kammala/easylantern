import logging
import os
import threading
import sys

from tornado.ioloop import IOLoop

from easylantern.ui import cliparams
from easylantern.ui.client.cli import CliClient


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('easylantern')

STOPPING_DELAY = 3

# # first try
# from tornado import gen
# from tornado.tcpclient import TCPClient
#
# @gen.coroutine
# def start():
#     print('Starting...')
#     client = TCPClient()
#     stream = yield client.connect('127.0.0.1', 9999)
#     print('Connected')
#     while True:
#         data = yield stream.read_bytes(3)
#         print('header: {}'.format(data, data[0]))
#         if data[0] == 0x20:
#             data = yield stream.read_bytes(3)
#             print('body: {}'.format(data))


class ClientThread(threading.Thread):
    def __init__(self, client_obj, host, port, *args, **kwargs):
        self.host = host
        self.port = port
        self.client = client_obj
        super(ClientThread, self).__init__(*args, **kwargs)

    def run(self):
        logger.debug('creating client')
        self.client.connect(host=self.host, port=self.port)
        IOLoop.instance().start()
        logger.debug('ioloop stopped')

if __name__ == '__main__':
    parser = cliparams.LanternParamParser(module='client')
    parser.add_argument('-d', '--stop-delay', dest='delay', default=STOPPING_DELAY, type=int)
    parser.add_argument('--ui', default='cli', choices=['cli', 'qt'])
    params = parser.parse_args()
    cliparams.configLogger(logger, params.verbose, params.log)

    logger.info('Running client on: {host}:{port}'.format(host=params.host, port=params.port))

    ecode = 0

    # ui
    if params.ui == 'qt':
        from PyQt5.QtCore import QUrl
        from PyQt5.QtGui import QGuiApplication
        from PyQt5.QtQml import QQmlApplicationEngine

        from easylantern.ui.client import qt
        from easylantern.ui.client.qt import QtGuiClient

        client = QtGuiClient()
        app = QGuiApplication(sys.argv)

        eng = QQmlApplicationEngine(QUrl(os.path.join(os.path.dirname(os.path.abspath(qt.__file__)),
                                                      'resources/lantern.qml')))
        eng.quit.connect(app.quit)
        ctx = eng.rootContext()
        ctx.setContextProperty('stateEmitter', client.emitter)
        # creating thread for async loop working. main thread remains for gui
        th = ClientThread(client, params.host, params.port)
        th.start()
        # ui blocks execution until it will be closed
        ecode = app.exec_()
        # exiting
        client.stop_loop()
        IOLoop.instance().stop()
        th.join(params.delay)
        if th.is_alive():
            logger.debug('Smth goes wrong: can not stop thread')
            # interrupt?
            # use multiprocessing?
            th.join()
        else:
            logger.debug('Successfully stopped')
        sys.exit(ecode)
    elif params.ui == 'cli':
        client = CliClient()
        # client.connect(host=params.host, port=params.port)
        # try:
        #     IOLoop.instance().start()
        #     ecode = -1
        # except KeyboardInterrupt:
        #     client.stop_loop()
        #     IOLoop.instance().stop()
        #     ecode = 0
    else:
        sys.exit(1)

    ecode = client(params.host, params.port, params.delay)
    sys.exit(ecode)