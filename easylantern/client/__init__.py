import logging
import time

from tornado import gen
from tornado.ioloop import IOLoop
from tornado.iostream import StreamClosedError
from tornado.tcpclient import TCPClient

from easylantern import proto
from easylantern.proto.commands import *

DEFAULT_PORT = 9999
DEFAULT_HOST = '127.0.0.1'
DEFAULT_COLOR = (255, 255, 255)

POLLING_INTERVAL = 3

logger = logging.getLogger(__name__)


class Client:
    def __init__(self):
        self.client = TCPClient()
        self.stream = None
        self.state = {
            'is_turned': False,
            'color': DEFAULT_COLOR
        }
        self.stopped = False
        self.last_host = None
        self.last_port = None

    @gen.coroutine
    def connect(self, host=DEFAULT_HOST, port=DEFAULT_PORT):
        self.last_host = host
        self.last_port = port
        while not self.stopped:
            try:
                self.stream = yield self.client.connect(host, port)
                break
            except ConnectionError as ex:
                logger.debug("Can't connect to server: {}".format(ex))
                yield gen.Task(IOLoop.instance().add_timeout, time.time() + POLLING_INTERVAL)
        logger.info('Connected to {}:{}'.format(host, port))
        yield self.parse_loop()

    @gen.coroutine
    def parse_loop(self):
        logger.debug('Starting endless loop')
        while not self.stopped:
            try:
                header = yield self.stream.read_bytes(proto.HEADER_SIZE)
                try:
                    command, body_length = proto.load(header)
                    if not command:
                        logger.debug('Incomplete command, waiting for {} bytes of body'.format(body_length))
                        body = yield self.stream.read_bytes(body_length)
                        command, body_length = proto.load(header+body)
                    logger.info('Got command "{}"'.format(command))
                    self.process(command)
                except proto.UnknownCommand as ex:
                    logger.info(ex)
                    logger.debug('Ignoring next {} bytes'.format(ex.body_length))
                    _ = yield self.stream.read_bytes(ex.body_length)
            except StreamClosedError:
                logger.info('Server closed connection')
                yield self.connect(self.last_host, self.last_port)

    def stop_loop(self):
        # i suggest that this operation is atomic
        self.stopped = True
        if self.stream and not self.stream.closed():
            self.stream.close()
        logger.debug('Stream closed')

    def process(self, command):
        """ Process command to update client state and run concrete realization visualization function. """
        if isinstance(command, On):
            self.state['is_turned'] = True
        elif isinstance(command, Off):
            self.state['is_turned'] = False
        elif isinstance(command, Color):
            self.state['color'] = command.body
        else:
            logger.warn('Not implemented command: {}'.format(command))
        self.visualize_state()

    def visualize_state(self):
        raise NotImplementedError()

    def __call__(self, host, port, delay):
        """ Main entry point for client """
        raise NotImplementedError()