from tornado.ioloop import IOLoop
from easylantern.client import Client


class CliClient(Client):
    def visualize_state(self):
        print('{} :: #{:02x}{:02x}{:02x}'.format('ON' if self.state['is_turned'] else 'OFF', *self.state['color']))

    def __call__(self, host, port, delay):
        """ Main entry point for client """
        self.connect(host, port)
        try:
            IOLoop.instance().start()
            ecode = -1
        except KeyboardInterrupt:
            self.stop_loop()
            IOLoop.instance().stop()
            ecode = 0
        return ecode