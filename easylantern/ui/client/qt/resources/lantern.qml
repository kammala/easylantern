import QtQuick 2.2
import QtQuick.Window 2.1

Window {
    visible: true
    width: 360
    height: 360
    color: "#202020"
    title: "Lantern"

    Text {
        id: state_label
        x: 91
        y: 233
        width: 60
        height: 28
        color: "dimgray"
        text: qsTr("State:")
        font.bold: true
        font.pixelSize: 21
    }

    Rectangle {
        id: state_indicator
        x: 166
        y: 233
        width: 28
        height: 28
        radius: 0.5 * width
        border.color: "black"
        border.width: 2
        gradient: Gradient {
            GradientStop { id: indicator_color; position: 0; color: "yellow" }
            GradientStop { position: 1; color: "black" }
        }
    }

    Text {
        id: state_text
        x: 200
        y: 233
        width: 28
        height: 28
        color: "dimgray"
        text: "no"
        font.pixelSize: 21
        MouseArea {
            id: toggle
            anchors.fill: parent
            onClicked: stateGroup.state = 'turned_on'
        }
    }

    Text {
        id: color_label
        x: 91
        y: 274
        width: 60
        height: 28
        text: qsTr("Color:")
        color: "dimgray"
        font.bold: true
        font.pixelSize: 21
    }

    Rectangle {
        id: color_sample
        x: 166
        y: 274
        width: 28
        height: 28
        color: "#00ffffff"
        border.width: 2
        radius: 5
    }

    Text {
        id: color_text
        x: color_sample.x + color_sample.width + color_sample.border.width + 5
        y: 274
        width: 56
        height: 28
        text: qsTr("unset")
        font.pixelSize: 21
        color: "dimgray"
    }

    Rectangle {
        id: lantern_back
        x: 80
        y: 8
        width: 200
        height: 200
        color: "#323232"
        radius: 5
        border.width: 2
        border.color: "black"

        Item {
            id: lantern
            x: parent.width/4
            y: parent.height/4
            width: parent.width/2
            height: parent.height/2

            Rectangle {
                id: lantern_ground
                x: 38
                y: 47
                width: 25
                height: 25
                radius: 5
            }
            Rectangle {
                id: lantern_ground_opacity
                x: lantern_ground.x - 1
                y: lantern_ground.y - 1
                color: "dimgray"
                width: lantern_ground.width + 2
                height: lantern_ground.height + 2
                radius: lantern_ground.radius
            }

            Rectangle {
                id: lantern_body
                x: 19
                y: 1
                width: 63
                height: 63
                radius: width * 0.5
            }
            Rectangle {
                id: lantern_body_opacity
                x: lantern_body.x - 1
                y: lantern_body.y - 1
                color: "dimgray"
                width: lantern_body.width + 2
                height: lantern_body.height + 2
                radius: width * 0.5
            }
        }
    }

    StateGroup {
            id: stateGroup
            states: [
                State {
                    name: "turned_on"

                    PropertyChanges {
                        target: indicator_color
                        color: "green"
                    }
                    PropertyChanges {
                        target: state_text
                        text: qsTr("On")
                    }
                    PropertyChanges {
                        target: lantern_ground_opacity
                        opacity: 0
                    }
                    PropertyChanges {
                        target: lantern_body_opacity
                        opacity: 0
                    }
                },
                State {
                    name: "turned_off"

                    PropertyChanges {
                        target: indicator_color
                        color: "red"
                    }
                    PropertyChanges {
                        target: state_text
                        text: qsTr("Off")
                    }
                    PropertyChanges {
                        target: lantern_ground_opacity
                        opacity: 0.7
                    }
                    PropertyChanges {
                        target: lantern_body_opacity
                        opacity: 0.7
                    }
                }
            ]
    }

    Item {
        Connections {
            target: stateEmitter
            onColorChanged: {
                lantern_body.color = color_str;
                lantern_ground.color = color_str;

                color_text.text = color_str;
                color_sample.color = color_str;
            }
            onTurnToggled: stateGroup.state = is_turned ? "turned_on" : "turned_off"
        }
    }

}
