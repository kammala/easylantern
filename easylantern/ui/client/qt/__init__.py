import logging
import os
import threading
import sys
from tornado.ioloop import IOLoop
from easylantern.client import Client

import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtQml as QtQml

OFF_OPACITY = 50
ON_OPACITY = 250

logger = logging.getLogger(__name__)


class StateChangingEmitter(QtCore.QObject):
    color_changed = QtCore.pyqtSignal('QString', name='colorChanged', arguments=['color_str'])
    turn_toggled = QtCore.pyqtSignal(bool, name='turnToggled', arguments=['is_turned'])


class QtGuiClient(Client):
    def __init__(self):
        self.emitter = StateChangingEmitter()
        super(QtGuiClient, self).__init__()

    def __call__(self, host, port, delay):
        """
        DEPRECATED. Cause of Qt specific behavior QGuiApplication.exec_ should be run DIRECTLY from main.
        I didn't found any proof of it, but it can be easy reproduced with smth like this:
        ```
            import sys

            from PyQt5.QtCore import QUrl
            from PyQt5.QtGui import QGuiApplication
            from PyQt5.QtQml import QQmlApplicationEngine

            def somefunc():
                app = QGuiApplication(sys.argv)
                eng = QQmlApplicationEngine(QUrl('test.qml'))
                eng.quit.connect(app.quit)
                return app.exec_()

            if __name__ == '__main__':
                # # This code would work
                # app = QGuiApplication(sys.argv)
                # eng = QQmlApplicationEngine(QUrl('test.qml'))
                # eng.quit.connect(app.quit)

                # sys.exit(app.exec_())

                # # instead of that
                sys.exit(somefunc())
        ```
        BTW there is no bug with old-style QMainWindow and QApplication.
        """
        # initialize gui
        app = QtGui.QGuiApplication(sys.argv)
        eng = QtQml.QQmlApplicationEngine(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                       'resources/lantern.qml'))
        eng.quit.connect(app.quit)
        ctx = eng.rootContext()
        ctx.setContextProperty('stateEmitter', self.emitter)

        # creating thread for async loop working. main thread remains for gui
        th = threading.Thread(target=self._run, args=[host, port], daemon=True)
        th.start()

        # will not work
        ecode = app.exec_()

        self.stop_loop()
        IOLoop.instance().stop()
        th.join(delay)
        if th.is_alive():
            logger.debug('Smth goes wrong: can not stop thread')
            # interrupt?
            # use multiprocessing?
            th.join()
        else:
            logger.debug('Successfully stopped')
        return ecode

    def _run(self, host, port):
        """ Runs async loop. This function used as target for running in separate Thread. """
        logger.debug('creating client')
        self.connect(host, port)
        IOLoop.instance().start()
        logger.debug('ioloop stopped')

    def visualize_state(self):
        self.emitter.turn_toggled.emit(self.state['is_turned'])
        # use QColor?
        self.emitter.color_changed['QString'].emit('#{:02x}{:02x}{:02x}'.format(*self.state['color']))