import functools
import logging
import threading
import re

from tornado.ioloop import IOLoop

from easylantern.proto import SUPPORTED_TYPES
from easylantern.proto.commands import *
from easylantern.server import EasyLanternServer

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class CommandError(Exception): pass


class InteractiveServerCLI:
    """ EasyLantern server realization with interactive command line interface. """
    def __init__(self, *, host, port, prompt='>'):
        self.prompt = prompt
        self.commands = {
            key: functools.partial(val, self)
            for key, val in self.__class__.__dict__.items()
            if key[0] != '_' and callable(val)
        }
        self.server = EasyLanternServer(host, port)
        self.server_thread = None

    def exit(self):
        """ Stop server and exit """
        print('Exiting')
        self.server.stop()
        IOLoop.instance().stop()
        return object()

    def on(self):
        """ Sends protocol ON command. """
        self.proto('on')

    def off(self):
        """ Sends protocol OFF command. """
        self.proto('off')

    def color(self, *args):
        """ Sends protocol COLOR command with 1 string(#hex format), or 3 int or float parameters. """
        if len(args) == 1 and isinstance(args[0], str):
            self.proto('color', args[0])
        elif len(args) == 3:
            try:
                self.proto('color', *[int(a) for a in args])
            except ValueError:
                try:
                    self.proto('color', *[float(a) for a in args])
                except ValueError:
                    raise CommandError('Incorrect type of parameters for "color": {}'.format(args))
        else:
            raise CommandError('Incorrect parameters for "color": {}'.format(args))

    def proto(self, command_name, *args):
        """ Sends protocol command by its type name without argument preparation. """
        command_cls = [c for c in SUPPORTED_TYPES.values() if c.__name__ == command_name.capitalize()][0]
        if not command_cls:
            raise CommandError('Unknown command: {}'.format(command_name))
        try:
            command = command_cls(*args)
            logger.debug('Sending command "{}" to client'.format(command))
            self.server.send(command)
        except AssertionError as e:
            raise CommandError(e)

    def help(self):
        """ Prints this message """
        for name in self.commands:
            command = getattr(self, name)
            print('* {} -- {}'.format(name, command.__doc__))

    def _run(self):
        """ Starts server and IOLoop. Should be run into separate thread """
        self.server.start()
        logger.debug('Starting async loop')
        IOLoop.instance().start()

    def __call__(self):
        """ Starts async loop thread and run command loop """
        self.server_thread = threading.Thread(target=self._run)
        self.server_thread.start()

        self._command_loop()

    def _command_loop(self):
        print('Welcome to EasyLantern Server CLI!')
        print('Enter "exit" to quit or "help" to list all commands')
        while True:
            try:
                command = input(self.prompt)
            except EOFError:
                command = 'exit'
            command, *args = re.split(r'\s+', command.lower())
            if command in self.commands:
                try:
                    result = self.commands[command](*args)
                    if result:
                        break
                except CommandError as ex:
                    logger.error(ex)
                    print('Error!', ex)
                except TypeError as ex:
                    print('Error! Incorrect parameters list for {}'.format(command))
                    logger.error('Incorrect parameters list for {}'.format(command))
                    logger.debug('Original exc: {}'.format(ex))
            else:
                logger.error('Unknown command "{}". Try "help" to see list of all available commands'.format(command))
