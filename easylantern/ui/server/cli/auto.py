import logging
import random
import time

from tornado import gen
from tornado.ioloop import IOLoop

from easylantern.proto.commands import On, Off, Color
from easylantern.server import EasyLanternServer

logger = logging.getLogger(__name__)


class AutoServerCLI:
    """ EasyLantern server realization that send random commands to client. """
    def __init__(self, *, host, port, sleep_interval):
        self.sleep_interval = sleep_interval
        self.server = EasyLanternServer(host, port)
        self.is_stopped = False

    def __call__(self):
        self._command_loop()
        logger.debug('Starting async loop')
        try:
            IOLoop.instance().start()
        except KeyboardInterrupt:
            logger.debug('Stopping')
            self.is_stopped = True
            self.server.stop()
            IOLoop.instance().stop()

    @gen.coroutine
    def _command_loop(self):
        self.server.start()
        while not self.is_stopped:
            logger.debug('Waiting for {}...'.format(self.sleep_interval))
            yield gen.Task(IOLoop.instance().add_timeout, time.time() + self.sleep_interval)
            logger.debug('Generating command')
            command_cls = random.choice([On, Off, Color])
            if command_cls == Color:
                command = Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
            else:
                command = command_cls()
            logger.debug('Sending command: {}'.format(command))
            yield self.server.send(command)