""" Helper module for common command line arguments. """

import argparse
import logging

from ..client import DEFAULT_HOST, DEFAULT_PORT

DEFAULT_LOG_LEVEL = logging.INFO
MINIMUM_FILELOG_LEVEL = logging.INFO


class LanternParamParser(argparse.ArgumentParser):
    def __init__(self, *, module, **kwargs):
        kwargs.setdefault('prog', 'python -m easylantern.{}'.format(module))
        kwargs.setdefault('description', 'Test easylantern {} CLI'.format(module))
        kwargs.setdefault('usage', '%(prog)s [options]')
        kwargs.setdefault('add_help', False)

        super(LanternParamParser, self).__init__(**kwargs)

        self.add_argument('-h', '--host', help='address to bind to', default=DEFAULT_HOST)
        self.add_argument('-p', '--port', help='port to bind to', default=DEFAULT_PORT)

        self.add_argument('-v', '--verbose', action='count', default=(logging.CRITICAL - DEFAULT_LOG_LEVEL)//10)
        self.add_argument('-q', '--quiet', action='store_const', const=0, dest='verbose')

        self.add_argument('-?', '--help', action='help')
        self.add_argument('--log')


def configLogger(logger, verbosity, logfile=None):
        log_level = max(logging.CRITICAL - verbosity*10, logging.NOTSET)
        # disable default console logger
        logger.propagate = False
        console = logging.StreamHandler()
        console.setLevel(log_level)
        console.setFormatter(logging.Formatter('%(message)s'))
        logger.addHandler(console)

        if logfile:
            filelog = logging.FileHandler(logfile)
            filelog.setLevel(min(MINIMUM_FILELOG_LEVEL, log_level))
            filelog.setFormatter(logging.Formatter('[%(asctime)s] %(levelname)-8s :: %(name)-12s :: %(message)s'))
            logger.addHandler(filelog)